import os
from llama_index.core import VectorStoreIndex, SimpleDirectoryReader, Settings
from llama_index.core.embeddings import resolve_embed_model
from llama_index.vector_stores.chroma import ChromaVectorStore
from llama_index.core import StorageContext
import os
from llama_index.readers.file import PDFReader
import torch
import chromadb
import pprint
from flatten_json import flatten
import pickle
import chainlit as cl
from dotenv import load_dotenv
from chromadb import Documents, EmbeddingFunction, Embeddings
from chromadb.utils import embedding_functions
from llama_index.llms.ollama import Ollama
from llama_index.embeddings.ollama import OllamaEmbedding
from llama_index.core.base.embeddings.base import BaseEmbedding
from llama_index.core.memory import ChatMemoryBuffer





#GLOBAL VARIABLES
#archive_path = "../../files"
host = 'localhost'
port = 8000
db_name ="db_tests_engbase_ollama"



## CONNECT TO THE CHROMADB DOCKER
try:
    
    chroma_client = chromadb.HttpClient(host=host, port=port)
    try: 
        chroma_collection = chroma_client.get_collection(db_name)
    except Exception as e:
        print(f"Error during the connection to collection {db_name}  Details of the error: {e}")

except Exception as e:
    print(f"Error during the connection to {host}:{port}")

chroma_collection.count()
## DECLARING MODELS ROLE


my_local_model_indexing = "llama3"

my_local_model_retrieval = "llama3"
# Crea un'istanza di Ollama con il modello locale
ollama_instance_indexing = Ollama(model=my_local_model_indexing, request_timeout=60.0,model_kwargs={"n_gpu_layers": 40})
ollama_instance_retrieval = Ollama(model=my_local_model_retrieval, request_timeout=60.0,model_kwargs={"n_gpu_layers": 40})
# Settings.llm = ollama_instance
# Settings.embed_model = ollama_instance

emb = OllamaEmbedding(model_name=my_local_model_indexing)
isinstance(emb, BaseEmbedding)

vector_store = ChromaVectorStore(chroma_collection=chroma_collection)
storage_context = StorageContext.from_defaults(vector_store=vector_store)
index = VectorStoreIndex.from_vector_store(
    vector_store,
    storage_context=storage_context,
    embed_model=emb
)




load_dotenv()



##LOAD TGHE INDEX


@cl.on_chat_start
async def factory():
    
    Settings.chunk_size =4096

        


    print("A new chat session has started!")
    memory = ChatMemoryBuffer.from_defaults(token_limit=96000)
    chat_engine = index.as_chat_engine(
        # service_context=service_context,
        chat_mode="condense_plus_context",
        memory=memory,
        llm=ollama_instance_retrieval,
        context_prompt=(
        "You are a Herbert Werner Frank, able to have normal interactions, as well as talk, you have been traine from original data, documents and papers from Herbert Werner Franke and you are him"
        "Your are a ChatBot expert about Herbert Werner Franke and talks like him"
        "Here are the relevant documents for the context:\n"
        "{context_str}"
        "\nInstruction: Use the previous chat history, or the context above, to interact and help the user and cite always the name of the document you are referring to using the field signatureFull of the node taken from the metadata of the retireved nodes and also at the end of each message you rewite the references, citancions and names of the documents, metadata, signatureFull you used to give an answer"
    ),
       #chat_mode ="context"
    )

    cl.user_session.set("chat_engine", chat_engine)


@cl.on_message
async def main(message: cl.Message):
    msg = cl.Message(content="")
    await msg.send()
    chat_engine = cl.user_session.get("chat_engine")


    
    response = await cl.make_async(chat_engine.chat)(message.content)
    print(response)
    response_message = cl.Message(content=f"{response}")
    await response_message.send()

    
