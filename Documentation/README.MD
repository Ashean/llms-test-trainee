# ChatBot
## A complete explanations 
This Readme.md document aims to provide a detailed explanation of the development and code behind a **chatbot developed using ChainLint**. The chatbot enables end-users to engage in **conversations with the content extracted from documents processed by locally hosted LLMs (Large Language Models) provided by Ollama**.

## References
- [Ollama documentation](https://ollama.com/)
- [HugginFace](https://huggingface.co/)
- [Llama3 on Ollama](https://ollama.com/library/llama3)
- [Chainlit](https://docs.chainlit.io/get-started/overview)
- [LlamaIndex](https://www.llamaindex.ai/)
- [ChromaDB](https://www.trychroma.com/)

## The used frameworks

- **ChromaDB**: ChromaDB is a versatile document storage and retrieval system designed to efficiently manage vast repositories of textual data. It serves as a central hub for storing documents of various formats, including PDFs, text files, and more, making them easily accessible for retrieval and analysis. With ChromaDB, developers can leverage a range of functionalities to interact with document collections, including querying based on content similarity, metadata attributes, and embedded embeddings.
By utilizing ChromaDB within the LlamaIndex framework, developers gain access to a powerful document management system that seamlessly integrates with other components of their applications. LlamaIndex abstracts away the complexities of interacting with ChromaDB, providing developers with intuitive interfaces and utilities to retrieve, process, and analyze documents stored within the database.
Furthermore, ChromaDB's support for embedding functions enables developers to represent documents as dense vectors, facilitating efficient similarity-based retrieval and clustering operations. This functionality is particularly useful in applications where document similarity or relatedness is a key factor, such as information retrieval, recommendation systems, and content summarization

- **LlamaIndex**:LlamaIndex is a comprehensive framework designed to streamline the development of systems leveraging Retrieve and Generate (RAG) approaches. This framework serves as a powerful tool for interfacing with both Ollama and the ChromaDB, facilitating seamless integration and interaction with these platforms. By utilizing LlamaIndex, developers can harness the capabilities of Ollama for advanced natural language processing tasks while also leveraging the rich document repository stored in the ChromaDB.
At its core, LlamaIndex simplifies the process of interfacing with Ollama, an AI model serving as the backbone for various linguistic analysis tasks. With LlamaIndex, developers gain access to a suite of functionalities and utilities tailored to enhance the efficiency and effectiveness of utilizing Ollama within their applications. Additionally, LlamaIndex provides seamless connectivity to the ChromaDB, enabling easy retrieval and management of documents for contextual analysis and information extraction.
In essence, LlamaIndex empowers developers to build sophisticated RAG systems with ease, thanks to its robust integration with Ollama and the ChromaDB. By abstracting away the complexities of interfacing with these platforms, LlamaIndex accelerates the development process and enables the creation of powerful natural language processing solutions tailored to diverse use cases and applications.

- **Chainlit** : Chainlit is an open-source Python package to build production ready Conversational AI.

- **Ollama** : the code elaborates on the utilization of Ollama within the context of managing locally downloaded models. Ollama serves as a pivotal component, facilitating seamless interaction with these models through the LlamaIndex library. By harnessing Ollama's capabilities, users can efficiently navigate and leverage the functionalities of the downloaded models for diverse natural language processing tasks. This integration streamlines the deployment and utilization of models, empowering users with enhanced control and accessibility to Ollama's resources for effective linguistic analysis and processing.

- **HuggingFace** : Hugging Face is a leading platform for natural language processing (NLP) that offers a wide range of pre-trained models, datasets, and tools to support NLP research and application development. It provides developers with access to state-of-the-art models for various NLP tasks, such as text classification, named entity recognition, question answering, and more.
Within the LlamaIndex framework, Hugging Face's models are leveraged to enhance the capabilities of the system in processing and understanding natural language text. By integrating Hugging Face's models into LlamaIndex, developers can tap into the vast array of pre-trained models available on the platform, allowing for efficient and effective natural language understanding.
Moreover, Hugging Face's ecosystem includes tools for fine-tuning models on custom datasets, enabling developers to adapt pre-trained models to specific tasks or domains. This flexibility makes it possible to tailor NLP models to suit the needs of different applications, ensuring optimal performance in real-world scenarios.

### Brief description of how framwroks work togheter
Within the integrated framework delineated in the provided code snippet, multiple components synergize to furnish the operational architecture of the chatbot system. This collaborative ecosystem amalgamates the prowess of several frameworks and tools, each serving a distinct yet interdependent role in ensuring the efficacy and functionality of the conversational AI system. At the core of this framework lies Ollama, a pivotal facilitator that furnishes access to potent language models tailored for indexing and retrieval tasks. Ollama empowers developers with the ability to leverage cutting-edge models, such as Llama3, enabling robust processing and comprehension of natural language text. The instantiation of Ollama instances, namely ollama_instance_indexing and ollama_instance_retrieval, with locally hosted models underscores the system's adaptability and customization to specific requirements. Complementing Ollama's capabilities, Hugging Face contributes a rich repertoire of pre-trained models and NLP tools, augmenting the chatbot's functionality with advanced language processing capabilities. Leveraging Hugging Face's models, particularly through the OllamaEmbedding class, facilitates tasks such as semantic similarity computation and contextual understanding, enriching the chatbot's interactions with users. LlamaIndex bridges the gap between Ollama and ChromaDB, facilitating seamless integration and interaction between these components. By leveraging LlamaIndex's functionalities, developers can harness Ollama's language models within the context of ChromaDB, a robust document storage and retrieval system. The VectorStoreIndex class empowers developers to index and query documents based on their embeddings, thereby leveraging Ollama's language models alongside ChromaDB's document management capabilities. Chainlit serves as the orchestration framework, providing the scaffolding for chat session management and message processing logic. It streamlines interactions between users and the chatbot, facilitating seamless communication and response generation. With Chainlit, developers can define the chatbot's behavior and logic, enabling it to engage users intelligently and effectively. ChromaDB underpins the document storage and retrieval aspect of the system, enabling efficient management and querying of structured and unstructured data. Integration with ChromaDB empowers developers to store, retrieve, and analyze documents relevant to the chatbot's domain, thereby enriching the conversational experience with valuable information and insights. In summary, the harmonious collaboration between Ollama, Hugging Face, LlamaIndex, Chainlit, and ChromaDB culminates in a robust ecosystem for developing sophisticated conversational AI systems. Each component contributes its unique capabilities, collectively enabling the creation of chatbots that are adept, adaptable, and efficient in meeting users' diverse needs across various domains and use cases.

#### Requirements:

CUDA support for accelerated processing.

Pre-existing embedded data saved within the vector database.

**Description:**

This code example assumes the prior creation of a collection in ChromaDB containing the embedded data of documents. It is essential to remember the dimensions of the embeddings during this process.

## Code
#### The aim of this description
The presented code exemplifies a comprehensive yet straightforward implementation of the aforementioned frameworks to develop a conversational agent capable of engaging with the contents of ZKM archives. Within this framework, a symbiotic relationship emerges among various components, each meticulously selected to synergize and empower the chatbot system.

#### Imports
```python
import os
from llama_index.core import VectorStoreIndex, SimpleDirectoryReader, Settings
from llama_index.core.embeddings import resolve_embed_model
from llama_index.vector_stores.chroma import ChromaVectorStore
from llama_index.core import StorageContext
import os
from llama_index.readers.file import PDFReader
import torch
import chromadb
import pprint
from flatten_json import flatten
import pickle
import chainlit as cl
from dotenv import load_dotenv
from chromadb import Documents, EmbeddingFunction, Embeddings
from chromadb.utils import embedding_functions
from llama_index.llms.ollama import Ollama
from llama_index.embeddings.ollama import OllamaEmbedding
from llama_index.core.base.embeddings.base import BaseEmbedding
from llama_index.core.memory import ChatMemoryBuffer
```

In this section, we include all the necessary imports for the libraries used. The package manager utilized in this project is Conda, and all packages can be installed using the following command:

```bash
    conda install [package_name1] [package_name2] ...

```

Replace [package_name1] [package_name2] ... with the names of the required packages.


#### Global Variables

```python

host = 'localhost'
port = 8000
db_name ="db_tests_engbase_ollama"
queried_metadata_mongodb = []

```
In this section, we define the global variables required for the development of the chatbot:

- **host**: Specifies the host address on where the ChromaDB is running, which in this case is set to 'localhost'. (Our CChromaDB runs in a Docker)

- **port**: Defines the port number of the ChromaDB, set to 8000.

- **db_name**: Specifies the name of the database being used, set to containing the emebedded documents.


#### Connect to the ChromaDB Docker
```python

## CONNECT TO THE CHROMADB DOCKER
try:
    
    chroma_client = chromadb.HttpClient(host=host, port=port)
    try: 
        chroma_collection = chroma_client.get_collection(db_name)
    except Exception as e:
        print(f"Error during the connection to collection {db_name}  Details of the error: {e}")

except Exception as e:
    print(f"Error during the connection to {host}:{port}")

chroma_collection.count()

```

This code snippet attempts to establish a connection to a ChromaDB instance hosted locally. Here's a breakdown of what the code does and the purpose of the variables and functions used:


1) **ChromaDB Connection**:
    - chroma_client: An instance of chromadb.HttpClient used to communicate with the ChromaDB server.
    - host: The hostname or IP address of the ChromaDB server.
    - port: The port number on which the ChromaDB server is running.

2) **Get Collection**:
    - chroma_collection: This variable stores the collection retrieved from ChromaDB using the get_collection method of the chroma_client.
    - db_name: The name of the database or collection to retrieve from ChromaDB.

3) **Error Handling**:
    - The code is wrapped in a try-except block to handle potential exceptions that may occur during the connection process.If an error occurs during the connection attempt (Exception as e), an error message is printed indicating the nature of the error.

4) **Chroma Collection Count**:
    - chroma_collection.count(): This line attempts to retrieve the count of documents or items in the chroma_collection.


#### Declare the  LLM's

```python

my_local_model_indexing = "llama3"

my_local_model_retrieval = "llama3"
# Crea un'istanza di Ollama con il modello locale
ollama_instance_indexing = Ollama(model=my_local_model_indexing, request_timeout=60.0,model_kwargs={"n_gpu_layers": 40})
ollama_instance_retrieval = Ollama(model=my_local_model_retrieval, request_timeout=60.0,model_kwargs={"n_gpu_layers": 40})
# Settings.llm = ollama_instance
# Settings.embed_model = ollama_instance

emb = OllamaEmbedding(model_name=my_local_model_indexing)
isinstance(emb, BaseEmbedding)


load_dotenv()

```
##### Model Configuration:  
my_local_model_indexing and my_local_model_retrieval: These variables define the model names used for indexing and retrieval. In this case, they are both set to "llama3".

##### Ollama Instance Initialization:
Two instances of the Ollama model are created for indexing (ollama_instance_indexing) and retrieval (ollama_instance_retrieval). These instances are initialized with the specified model name, timeout settings, and additional model arguments such as the number of GPU layers ("n_gpu_layers": 40).
    
##### Embedding Configuration:
An instance of OllamaEmbedding (emb) is created with the same model name used for indexing (my_local_model_indexing). This embedding instance is checked using isinstance to ensure it inherits from the BaseEmbedding class.
    
##### Explanation:
The llama3 model is a pre-installed model on the local Ollama server.
We configure and initialize the Ollama model instances for indexing and retrieval tasks in LlamaIndex.
By specifying the model name and additional parameters, we customize the behavior of the Ollama model for our specific use case within LlamaIndex.


#### Get the ChromaVector & define the Index
```python

vector_store = ChromaVectorStore(chroma_collection=chroma_collection)
storage_context = StorageContext.from_defaults(vector_store=vector_store)
index = VectorStoreIndex.from_vector_store(
    vector_store,
    storage_context=storage_context,
    embed_model=emb
)

```
This code segment is responsible for setting up the vector store, storage context, and creating an index using LlamaIndex with the ChromaDB collection. 

1)    Vector Store Initialization:
    -    vector_store: An instance of ChromaVectorStore is created, which serves as the vector store for the LlamaIndex. It is initialized with the ChromaDB collection (chroma_collection).

2)    Storage Context Configuration:
    -    storage_context: A storage context is created using the from_defaults method, which sets up default parameters. It includes the vector store (vector_store) created in the previous step.

3)    Index Creation:
    -    index: A vector store index is instantiated using the VectorStoreIndex.from_vector_store method. It takes the 
    vector store (vector_store), storage context (storage_context), and embedding model (emb) as parameters.
    - The embedding model (emb) is provided to the index, ensuring that the indexed vectors are associated with the appropriate embeddings.

###### Explanation:
The ChromaVectorStore serves as the storage backend for the indexed vectors.
The storage context provides configuration parameters and manages interactions between the index and the vector store.
The index is created using the vector store and storage context, ensuring that it can efficiently retrieve and manipulate the indexed data.
By specifying the embedding model, we associate the indexed vectors with their corresponding embeddings, enabling similarity searches and other operations based on embeddings.

#### On chatbot start
```python

@cl.on_chat_start
async def factory():
    
    Settings.chunk_size =4096

    print("A new chat session has started!")
    memory = ChatMemoryBuffer.from_defaults(token_limit=96000)
    chat_engine = index.as_chat_engine(
        # service_context=service_context,
        chat_mode="condense_plus_context",
        memory=memory,
        llm=ollama_instance_retrieval,
        context_prompt=(
        "You are a Herbert Werner Frank, able to have normal interactions, as well as talk, you have been traine from original data, documents and papers from Herbert Werner Franke and you are him"
        "Your are a ChatBot expert about Herbert Werner Franke and talks like him"
        "Here are the relevant documents for the context:\n"
        "{context_str}"
        "\nInstruction: Use the previous chat history, or the context above, to interact and help the user and cite always the name of the document you are referring to using the field signatureFull of the node taken from the metadata of the retireved nodes and also at the end of each message you rewite the references, citancions and names of the documents, metadata, signatureFull you used to give an answer"
    ),
       #chat_mode ="context"
    )

    cl.user_session.set("chat_engine", chat_engine)

```   
This function is executed whenever a new chat session starts. It performs initialization tasks and sets up the chat engine for interaction.

1)    Settings Configuration:
        Settings.chunk_size = 4096: Sets the chunk size for processing messages. This configuration may affect how messages are processed or segmented.

2)    Chat Session Initialization:
        Prints a message indicating the start of a new chat session.
        Initializes a ChatMemoryBuffer object with default settings to manage chat memory.
        Creates a chat engine using the index.as_chat_engine() method, configuring it with specific settings.

3)    Chat Engine Configuration:
        chat_mode="condense_plus_context": Sets the chat mode to "condense_plus_context," which likely configures the chat engine to handle context and condensation during conversations.
        Configures memory, LLama instance, and context prompt for the chat engine.

4)    User Session Management:
        Stores the chat engine instance in the user session using cl.user_session.set() method. This allows for easy retrieval and management of the chat engine instance during the chat session.


#### What do do when a message is received?
```python

@cl.on_message
async def main(message: cl.Message):
    msg = cl.Message(content="")
    await msg.send()
    chat_engine = cl.user_session.get("chat_engine")


    
    response = await cl.make_async(chat_engine.chat)(message.content)
    print(response)
    response_message = cl.Message(content=f"{response}")
    await response_message.send()

```   
This function is executed whenever a message is received during a chat session. It handles incoming messages and generates responses using the configured chat engine.

1) Message Handling:
        Upon receiving a message (message: cl.Message), the function initializes a new cl.Message object msg with an empty content and sends it. This step may serve as a way to acknowledge receipt of the message or to trigger some actions.

2)    Chat Engine Retrieval:
        Retrieves the chat engine instance from the user session using cl.user_session.get("chat_engine"). This allows access to the configured chat engine for generating responses.

3)    Response Generation:
        Uses the retrieved chat engine to asynchronously generate a response to the received message. This is achieved by calling cl.make_async(chat_engine.chat)(message.content), where message.content contains the content of the received message.

4)    Printing and Sending Response:
        Prints the generated response to the console.
        Initializes a new cl.Message object response_message containing the generated response and sends it back to the user.

### Considerations about the code
The development of a chatbot system using programming languages and dedicated frameworks, as evidenced by the provided code, represents a crucial activity in today's software development landscape. This approach, while promising, presents both positive and negative aspects that need to be carefully evaluated to determine overall effectiveness.

Among the positive aspects, the flexibility and versatility offered by the utilized frameworks, such as LlamaIndex, ChromaDB, and Chainlit, stand out. These tools provide a wide range of predefined functionalities and specific modules that greatly simplify the development of the chatbot. Furthermore, the modularity of the code allows for easy extension and customization of the system to adapt it to the user's specific needs.

Another strength lies in the integration of advanced technologies, such as Ollama, for natural language management and processing. Thanks to these technologies, the chatbot can intelligently understand and respond to user requests, enhancing the overall interaction experience.

However, there are also some negative aspects to consider. Firstly, the complexity of the system may increase with the addition of new features or integration with other external services. This could make the code more difficult to maintain and update over time, especially if it is not adequately documented and structured.

Additionally, the efficiency of the system may be compromised by the need to perform real-time natural language processing operations, especially if the volume of user requests is high. This could lead to longer response times or even scalability issues in case of sudden traffic spikes.

Furthermore, when considering the use of local LLM systems, both advantages and disadvantages arise. On the positive side, having LLM systems locally installed allows for greater control over data privacy and security, as sensitive information does not need to be transmitted over the internet. Additionally, local deployment can potentially reduce latency and improve response times, especially for applications requiring real-time interactions.

However, there are also drawbacks to local LLM deployment. Managing and maintaining local infrastructure can be more complex and resource-intensive compared to using cloud-based solutions. Additionally, local deployment may limit scalability, as the system's capacity is constrained by the hardware resources available locally.

Despite these challenges, it is important to highlight that implementing a chatbot using modern frameworks and technologies offers numerous opportunities to enhance human-machine interaction and optimize business processes. With careful design and resource management, it is possible to overcome challenges and maximize the benefits offered by this innovative solution.